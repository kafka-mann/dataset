# Dataset for Kafka-Mann Classification Task

The dataset for Kafka-Mann Classification Task consists of several books from
[Gutenberg.org](https://www.gutenberg.org). For Franz Kafka the following books
are used:

* *Das Urteil: Eine Geschichte by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/21593)
* *In der Strafkolonie by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/25791)
* *Die Verwandlung by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/22367)
* *Betrachtung by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/23532)
* *Grosser Lärm by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/30570)
* *Der Mord by Franz Kafka*, from [here](https://www.gutenberg.org/ebooks/30753)

Overall size is 262,4 kB.

For Thomas Mann the following books are used:

* *Der Tod in Venedig by Thomas Mann*, from [here](https://www.gutenberg.org/ebooks/12108)
* *Tristan by Thomas Mann*, from [here](https://www.gutenberg.org/ebooks/13810)

Overall size is  271,2 kB.

# Preprocessing

The following preprocessing steps are done for each author:

* concatenate all books into one text file
* remove Windows newlines
* end-of-sentence detection
* create a "one-sentence-per-line" file
* filter out sentences which are shorter than 8 words
* filter out duplicate sentences

Then training, development and test sets for each author can be created. For
more information see the `split_dataset.py` script [here](scripts/split_dataset.py).

# Dataset

The dataset for the Kafka-Mann Classification task is located in the `data`
folder of this repository.

The split ratio of training, development and test set is: 0.8, 0.1 and 0.1:

| Author      | Length training set | Length development set | Length test set
| ----------- | ------------------- | ---------------------- | ---------------
| Franz Kafka |        1226         |           137          |      152
| Thomas Mann |        1179         |           132          |      146

Summary:

| Dataset      | Length  | Filename
| ------------ | ------- | --------
| Training     |  2404   | `train`, see it [here](data/train)
| Development  |   269   | `dev`, see it [here](data/dev)
| Test         |   298   | `test`, see it [here](data/test.unlabeled)

## Description

The dataset description is very simple; each line of the training, development
or test sets consists of:

* label
* tabulator separator
* sentence

The following labels are assigned:

| Label | Author
| ----- | ------
| `0`   | Franz Kafka
| `1`   | Thomas Mann

The sentence is **not** tokenized. Here comes an example from the training set:

```text
0	Wie krachte in dem heißen Sommer das Holz in ihren Speichen und Deichseln!
```

*Notice*: The test set is unlabeled due to the Classification Competition!

# Contact (Bugs, Feedback, Contribution and more)

For questions about the dataset repository, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
the data and software in this repository is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.
