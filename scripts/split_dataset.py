from sklearn.model_selection import train_test_split
import string
import sys

# First argument must be the filename for the Kafka corpus.
# We expect a "one-sentence-per-line" input file here.
kafka_filename = sys.argv[1]

# Second argument must be the filename for the Mann corpus.
# We also expect a "one-sentence-per-line" input file here.
mann_filename  = sys.argv[2]

def read_corpus(filename):
    with open(filename) as f:
        # A minimum sentence length of 8 tokens is required
        # We also avoid duplicate sentences here
        corpus = list(set([line.strip() for line in f.readlines() if len(line.split(" ")) > 8]))
        return corpus

def split_corpus(corpus):
    train, test = train_test_split(corpus, test_size = 0.1)

    train, dev = train_test_split(train, test_size = 0.1)

    return train, dev, test

def label_dataset(label, dataset):
    dataset = ["{}\t{}".format(label, line) for line in dataset]
    return dataset

def write_dataset(name, dataset):
    with open(name, 'w') as f:
        f.write("\n".join(dataset))

kafka_corpus = read_corpus(kafka_filename)
mann_corpus  = read_corpus(mann_filename)

kafka_train, kafka_dev, kafka_test = split_corpus(kafka_corpus)

print("Length Kafka training set:", len(kafka_train))
print("Length Kafka development set:", len(kafka_dev))
print("Length Kafka test set:", len(kafka_test))

mann_train, mann_dev, mann_test = split_corpus(mann_corpus)

print("Length Mann training set:", len(mann_train))
print("Length Mann development set:", len(mann_dev))
print("Length Mann test set:", len(mann_test))

# Labels:
# O = Franz Kafka
# 1 = Thomas Mann

kafka_train = label_dataset("0", kafka_train)
kafka_dev   = label_dataset("0", kafka_dev)
kafka_test  = label_dataset("0", kafka_test)

mann_train = label_dataset("1", mann_train)
mann_dev   = label_dataset("1", mann_dev)
mann_test  = label_dataset("1", mann_test)

write_dataset("train", kafka_train + mann_train)
write_dataset("dev",   kafka_dev   + mann_dev)
write_dataset("test",  kafka_test  + mann_test)
